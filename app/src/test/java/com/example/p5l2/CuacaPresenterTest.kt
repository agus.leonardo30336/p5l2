package com.example.p5l2

import org.junit.Test

import org.junit.Assert.*
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.mockito.Mockito.mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class CuacaPresenterTest {

    @Test
    fun testKonversiSuhu() {
        val view: CuacaVPInterface = mock(CuacaVPInterface::class.java)
        val presenter = CuacaPresenter(view)
        val suhu = presenter.konversiSuhu(274.15)
        assertEquals(0.0, suhu, 0.00001)
    }

    @Test
    fun testKonversiFormat(){
        val view: CuacaVPInterface = mock(CuacaVPInterface::class.java)
        val presenter = CuacaPresenter(view)
        val suhu = presenter.konversiFormat(28.32111)
        assertEquals("test konversi format", suhu, "28.32")
    }

    @Test
    fun tampilCuaca() {
        val view: CuacaVPInterface = Mockito.mock(CuacaVPInterface::class.java)
        val presenter = CuacaPresenter(view)

        val json = presenter.modelCuaca(
            "Medan",
            "Mendung",
            "Sedikit berawan",
            301.15,
            302.15
        )

        val model = CuacaModel("Medan",
            "Mendung",
            "Sedikit berawan",
            "27",
            "28")

        assertEquals("model", json, model)
    }

}