package com.example.p5l2

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.ArrayAdapter
import android.widget.Toast
import com.firebase.jobdispatcher.*
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {
    private val Tag = "MyDispatcherTag1"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSpinner()
        startJob.setOnClickListener { startDispatcher() }
        cancelJob.setOnClickListener { cancelDispatcher() }
    }

    private fun setSpinner() {
        val kota = arrayOf("Medan", "Jakarta", "Bandung")
        val spinnerArrayAdapter = ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, kota)
        spinnerArrayAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item)
        spinner.adapter = spinnerArrayAdapter
    }

    private fun startDispatcher() {
        val bundle = Bundle()
        bundle.putString("kota", spinner.selectedItem.toString())
        val mFirebaseJobDispatcher = FirebaseJobDispatcher(GooglePlayDriver(applicationContext))
        val myJobDispatcher = mFirebaseJobDispatcher.newJobBuilder()
            .setService(CuacaHariIni::class.java)
            .setTag(Tag)
            .setRecurring(true)
            .setLifetime(Lifetime.UNTIL_NEXT_BOOT)
            .setTrigger(Trigger.executionWindow(0, 30))
            .setReplaceCurrent(true)
            .setRetryStrategy(RetryStrategy.DEFAULT_EXPONENTIAL)
            .setConstraints(Constraint.ON_ANY_NETWORK)
            .setExtras(bundle)
            .build()
        mFirebaseJobDispatcher.mustSchedule(myJobDispatcher)
        Toast.makeText(this, "Job Dispatcher Berjalan", Toast.LENGTH_SHORT).show()
    }

    private fun cancelDispatcher() {
        val mFirebaseJobDispatcher = FirebaseJobDispatcher(GooglePlayDriver(applicationContext))
        mFirebaseJobDispatcher.cancel(Tag)
        Toast.makeText(this, "Job Dispatcher Berhenti", Toast.LENGTH_SHORT).show()
    }
}
