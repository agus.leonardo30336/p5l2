package com.example.p5l2

data class CuacaModel(
    var kota: String,
    var weatherMain: String,
    var weatherDescription: String,
    var tempMin: String,
    var tempMax: String
)