package com.example.p5l2

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.support.v4.app.NotificationCompat
import android.util.Log
import com.firebase.jobdispatcher.JobParameters
import com.firebase.jobdispatcher.JobService
import com.loopj.android.http.AsyncHttpClient
import com.loopj.android.http.AsyncHttpResponseHandler
import cz.msebera.android.httpclient.Header
import org.json.JSONObject
import java.text.DecimalFormat

class CuacaHariIni : JobService(), CuacaVPInterface {

    val TAG = CuacaHariIni::class.java.simpleName
    val AppID = "63971c559501b72e05f66964fda3e3d2"
    var presenter = CuacaPresenter(this)

    override fun tampilCuaca(model: CuacaModel) {

        val mBuilder = NotificationCompat.Builder(this@CuacaHariIni, "asd")
            .setSmallIcon(R.mipmap.ic_launcher)
            .setContentTitle("Cuaca ${model.kota} Hari ini")
            .setContentText(model.weatherMain)
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
            .setStyle(
                NotificationCompat.BigTextStyle()
                    .bigText("\nDeskripsi : ${model.weatherDescription.capitalize()}\nSuhu : ${model.tempMin} - ${model.tempMax}")
            )

        val notificationManager: NotificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val id = 30103

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            val channelId = "asd"
            val channelName = "test"
            val importance = NotificationManager.IMPORTANCE_LOW

            val mChannel = NotificationChannel(
                channelId, channelName, importance
            )
            notificationManager.createNotificationChannel(mChannel)
        }
        notificationManager.notify(id, mBuilder.build())
    }

    override fun onStartJob(job: JobParameters): Boolean {
        Log.d(TAG, "Start Job")
        getCuacaHariIni(job)
        return true
    }

    override fun onStopJob(job: JobParameters): Boolean {
        Log.d(TAG, "Stop Job")
        return true
    }

    private fun getCuacaHariIni(job: JobParameters?) {
        val kota = job!!.extras!!.getString("kota")
        val client = AsyncHttpClient()
        val url = "http://api.openweathermap.org/data/2.5/weather?q=$kota&APPID=$AppID"

        val charset = Charsets.UTF_8

        val handler = object : AsyncHttpResponseHandler() {
            override fun onSuccess(
                statusCode: Int, headers: Array<out Header>?,
                responseBody: ByteArray?
            ) {
                var result = ""

                if (responseBody != null) {
                    result = responseBody.toString(charset)

                    val obj = JSONObject(result)
                    val weatherArray = obj.getJSONArray("weather")
                    val weather = weatherArray.getJSONObject(0)
                    val weatherMain = weather.getString("main")
                    val weatherDescription = weather.getString("description")

                    val main = obj.getJSONObject("main")
                    val tempMin = main.getDouble("temp_min")
                    val tempMax = main.getDouble("temp_max")

                    presenter.inputCuaca(kota!!, weatherMain, weatherDescription, tempMin, tempMax)
                }
                Log.d(TAG, result)
                jobFinished(job, false)
            }

            override fun onFailure(
                statusCode: Int, headers: Array<out Header>?,
                responseBody: ByteArray?, error: Throwable?
            ) {
                jobFinished(job, true)
                Log.d(TAG, "Failed")
            }
        }
        client.get(url, handler)
    }
}