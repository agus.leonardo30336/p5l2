package com.example.p5l2

import java.text.DecimalFormat

class CuacaPresenter(setView: CuacaVPInterface) {
    private val view = setView

    fun konversiSuhu(suhu: Double): Double {
        return suhu - 274.15
    }

    fun konversiFormat(suhu: Double): String {
        val df = DecimalFormat("0.##")
        return df.format(suhu)
    }

    fun modelCuaca(kota: String, weatherMain: String, weatherDescription: String, tempMin: Double, tempMax: Double): CuacaModel {
        val modelCuaca = CuacaModel(
            kota,
            weatherMain,
            weatherDescription,
            konversiFormat(konversiSuhu(tempMin)),
            konversiFormat(konversiSuhu(tempMax))
        )
        return modelCuaca
    }

    fun inputCuaca(kota: String, weatherMain: String, weatherDescription: String, tempMin: Double, tempMax: Double) {
        view.tampilCuaca(
            modelCuaca(
                kota,
                weatherMain,
                weatherDescription,
                tempMin,
                tempMax
            )
        )
    }
}